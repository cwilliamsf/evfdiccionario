/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.controller.dao.ConsultadiccionarioJpaController;
import cl.controller.entity.Consultadiccionario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author camilo williams
 */
@WebServlet(name = "DiccionarioController", urlPatterns = {"/DiccionarioController"})
public class DiccionarioController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DiccionarioController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DiccionarioController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            String lista = request.getParameter("bton");
            String idpalabra = request.getParameter("idpalabra");
            
            
            if (lista.equals("palabra1")) { 
                
                try {
                    String id = request.getParameter("id");
                    String palabra = request.getParameter("palabra");
                    String signi = request.getParameter("significado");
                    String fecha = request.getParameter("fecha");
                    
                    Consultadiccionario consulta = new Consultadiccionario();
                    consulta.setId(id);
                    consulta.setPalabra(palabra);
                    consulta.setSignificado(signi);
                    consulta.setFecha(fecha);
                    
                    ConsultadiccionarioJpaController dao = new ConsultadiccionarioJpaController();
                    
                    dao.create(consulta);
                } catch (Exception ex) {
                    Logger.getLogger(DiccionarioController.class.getName()).log(Level.SEVERE, null, ex);
                  }
            }
            if (lista.equals("historial")) {
            ConsultadiccionarioJpaController dao = new ConsultadiccionarioJpaController();
            List<Consultadiccionario> historial1 = dao.findConsultadiccionarioEntities();
            request.setAttribute("consultahistorial", historial1);
            request.getRequestDispatcher("historial.jsp").forward(request, response);
            
            }
        }
    
    /* por solucionar online
                try {
                    Client client = ClientBuilder.newClient();
                    WebTarget oxdiccionario = client.target("por definir" + idpalabra);
                    
                    Consultadiccionario significado=(Consultadiccionario) oxdiccionario.request(MediaType.APPLICATION_JSON).get(new GenericType<Consultadiccionario>(){});
                    
                    String id = request.getParameter("id");
                    String palabra = request.getParameter("palabra");
                    String signi = request.getParameter("significado");
                    String fecha = request.getParameter("fecha");
                    
                    Consultadiccionario consulta = new Consultadiccionario();
                    consulta.setId(id);
                    consulta.setPalabra(palabra);
                    consulta.setSignificado(signi);
                    consulta.setFecha(fecha);
                    
                    ConsultadiccionarioJpaController dao = new ConsultadiccionarioJpaController();
                    
                    dao.create(consulta);
                } catch (Exception ex) {
                    Logger.getLogger(DiccionarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("", );
                request.getRequestDispatcher(".jsp").forward(request, response);
                */
    
    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

