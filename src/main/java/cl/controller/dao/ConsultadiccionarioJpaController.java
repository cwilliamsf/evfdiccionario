/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller.dao;

import cl.controller.dao.exceptions.NonexistentEntityException;
import cl.controller.dao.exceptions.PreexistingEntityException;
import cl.controller.entity.Consultadiccionario;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author camilo williams
 */
public class ConsultadiccionarioJpaController implements Serializable {

    public ConsultadiccionarioJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");


    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Consultadiccionario consultadiccionario) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(consultadiccionario);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findConsultadiccionario(consultadiccionario.getId()) != null) {
                throw new PreexistingEntityException("Consultadiccionario " + consultadiccionario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Consultadiccionario consultadiccionario) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            consultadiccionario = em.merge(consultadiccionario);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = consultadiccionario.getId();
                if (findConsultadiccionario(id) == null) {
                    throw new NonexistentEntityException("The consultadiccionario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Consultadiccionario consultadiccionario;
            try {
                consultadiccionario = em.getReference(Consultadiccionario.class, id);
                consultadiccionario.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The consultadiccionario with id " + id + " no longer exists.", enfe);
            }
            em.remove(consultadiccionario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Consultadiccionario> findConsultadiccionarioEntities() {
        return findConsultadiccionarioEntities(true, -1, -1);
    }

    public List<Consultadiccionario> findConsultadiccionarioEntities(int maxResults, int firstResult) {
        return findConsultadiccionarioEntities(false, maxResults, firstResult);
    }

    private List<Consultadiccionario> findConsultadiccionarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Consultadiccionario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Consultadiccionario findConsultadiccionario(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Consultadiccionario.class, id);
        } finally {
            em.close();
        }
    }

    public int getConsultadiccionarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Consultadiccionario> rt = cq.from(Consultadiccionario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
