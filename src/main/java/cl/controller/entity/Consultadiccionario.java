/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.core.GenericType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author camilo williams
 */
@Entity
@Table(name = "consultadiccionario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consultadiccionario.findAll", query = "SELECT c FROM Consultadiccionario c"),
    @NamedQuery(name = "Consultadiccionario.findById", query = "SELECT c FROM Consultadiccionario c WHERE c.id = :id"),
    @NamedQuery(name = "Consultadiccionario.findByPalabra", query = "SELECT c FROM Consultadiccionario c WHERE c.palabra = :palabra"),
    @NamedQuery(name = "Consultadiccionario.findBySignificado", query = "SELECT c FROM Consultadiccionario c WHERE c.significado = :significado"),
    @NamedQuery(name = "Consultadiccionario.findByFecha", query = "SELECT c FROM Consultadiccionario c WHERE c.fecha = :fecha")})
public class Consultadiccionario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "id")
    private String id;
    @Size(max = 2147483647)
    @Column(name = "palabra")
    private String palabra;
    @Size(max = 2147483647)
    @Column(name = "significado")
    private String significado;
    @Size(max = 2147483647)
    @Column(name = "fecha")
    private String fecha;

    public Consultadiccionario() {
    }

    public Consultadiccionario(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getSignificado() {
        return significado;
    }

    public void setSignificado(String significado) {
        this.significado = significado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consultadiccionario)) {
            return false;
        }
        Consultadiccionario other = (Consultadiccionario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.controller.entity.Consultadiccionario[ id=" + id + " ]";
    }

    public Consultadiccionario get(GenericType<Consultadiccionario> genericType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
