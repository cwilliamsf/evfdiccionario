/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionario;

import cl.controller.dao.ConsultadiccionarioJpaController;
import cl.controller.entity.Consultadiccionario;
import cl.dto.PalabraDTO;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author camilo williams
 */
@Path("diccionario")
public class ApiDiccionario {
    
    @GET
    @Path("/{idpalabra}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado(@PathParam("idpalabra") String idpalabra){
      
        ConsultadiccionarioJpaController dao=new ConsultadiccionarioJpaController();
        List<Consultadiccionario> historial1=dao.findConsultadiccionarioEntities();
    
    
    return Response.ok(200).entity(historial1).build();
    
    
    }
        
        
        
        
    }
    
    
    
    /* intento online por arreglar
    @GET
    @Path("/{idpalabra}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado(@PathParam("idpalabra") String idpalabra){
    
        try {
            Client client= ClientBuilder.newClient();
            WebTarget oxdiccionario= client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es" + idpalabra);
            
            PalabraDTO sigpalabra=oxdiccionario.request(MediaType.APPLICATION_JSON).header("app_id", "38da8f2c").header("app_key", "7dbabc720fc061a84f0e74428b9bac01").get(PalabraDTO.class);
            
            Consultadiccionario buscpal=new Consultadiccionario();
            
            buscpal.setPalabra(sigpalabra.getWord());
            Date fecha=new Date();
            buscpal.setFecha(fecha.toString());
            //buscpal.setSignificado(sigpalabra.);
            
            Random rand = new Random();
            int upperbound = 1000;
        
            int intRandom = rand.nextInt(upperbound); 
        
            String s = String.valueOf(intRandom);
            buscpal.setId(s);
            
            String significado = (String) sigpalabra.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
            buscpal.setSignificado(significado);
            
            ConsultadiccionarioJpaController dao = new ConsultadiccionarioJpaController();
            dao.create(buscpal);
            
            
            
            return Response.ok(200).entity(buscpal).build();
        } catch (Exception ex) {
            Logger.getLogger(ApiDiccionario.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        return null;
        
    } */
    
            
    

