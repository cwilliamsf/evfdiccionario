<%-- 
    Document   : historial
    Created on : 08-05-2021, 19:38:55
    Author     : camilo williams
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="cl.controller.entity.Consultadiccionario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Consultadiccionario> cdiccionario = (List<Consultadiccionario>) request.getAttribute("consultahistorial");
    Iterator<Consultadiccionario> itconsulta = cdiccionario.iterator();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Historial de Consultas</h1>
         <form  name="form" action="DiccionarioController" method="POST">

       
        <table border="1">
                    <thead>
                    <th>Id</th>
                    <th>Palabra</th>
                    <th>Significado</th>
                    <th>Fecha</th>
         
                    </thead>
                    <tbody>
                        <%while (itconsulta.hasNext()) {
                       Consultadiccionario cd = itconsulta.next();%>
                        <tr>
                            <td id="id1"><%= cd.getId()%></td>
                            <td id="palabra"><%= cd.getPalabra()%></td>
                            <td id="definicion"><%= cd.getSignificado()%></td>
                            <td id="fecha"><%= cd.getFecha()%></td>
                         
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
                    <br><br>
     
        
         </form>   
    </body>
</html>
